import { getEnvironmentString } from '@kominal/lib-node-environment';

export const PAPYRUS_BASE_URL = getEnvironmentString('PAPYRUS_BASE_URL', 'https://papyrus.kominal.app');
export const TENANT_ID = process.env.PAPYRUS_TENANT_ID;
export const PROJECT_NAME = process.env.PROJECT_NAME;
